OUTSIDE_CIRCLE = 98;
DEPTH = 10;
WEIGHT = 10;
GRILL_DEPTH = 2;
GRILL_HOLE = 2;
KNOB_BOLT_DIAMETER = 7;

$fn=100;

difference(){
    union(){
        difference(){
            // outside circle
            cylinder(r=OUTSIDE_CIRCLE/2,h=DEPTH);
            
            // inside circle
            translate([0,0,-1]){
                cylinder(r=(OUTSIDE_CIRCLE/2)-WEIGHT,h=DEPTH+2);
            }
        }

        // X
        translate([-(OUTSIDE_CIRCLE/2)+1,-WEIGHT/2,0]){
            cube([OUTSIDE_CIRCLE-2,WEIGHT,DEPTH]);
        }
        rotate([0,0,90]){
            translate([-(OUTSIDE_CIRCLE/2)+1,-WEIGHT/2,0]){
                cube([OUTSIDE_CIRCLE-2,WEIGHT,DEPTH]);
            }
        }    

        // grill
        difference(){
            cylinder(r=OUTSIDE_CIRCLE/2,h=GRILL_DEPTH);
            
            translate([-OUTSIDE_CIRCLE/2,-OUTSIDE_CIRCLE/2,-1]){
                // y looop
                for(j=[0:(OUTSIDE_CIRCLE/(GRILL_HOLE*2))]){      
                    // x loop
                    for(i=[0:(OUTSIDE_CIRCLE/(GRILL_HOLE*2))]){
                        translate([i * (GRILL_HOLE*2),j*(GRILL_HOLE*2),0]){
                            cube([GRILL_HOLE,GRILL_HOLE,DEPTH+2]);
                            //cylinder(r=GRILL_HOLE/2,h=DEPTH+2);
                        }
                    }
                }
            }
        }  
    }
    
    // knob hole
    translate([0,0,-1]){
        cylinder(r=KNOB_BOLT_DIAMETER/2,h=DEPTH+2);
    }
}