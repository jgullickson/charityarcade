VERSION = "V1";

$fn=50;

difference(){
    union(){
        difference(){
            
            // main body
            union(){
                cube([90,10,30]);
                translate([3.5,-3,0]){
                    cube([7,16,30]);
                }
            }
            
            // slot
            translate([2,2,2]){
                cube([90,6,30]);
            }
        }

        // mounting holes
        translate([7,-5,0]){
            difference(){
                cylinder(r=4,h=30);
                translate([0,0,-1]){
                    cylinder(r=4.2/2,h=32);
                }
            }
        }
        translate([7,15,0]){
            difference(){
                cylinder(r=4,h=30);
                translate([0,0,-1]){
                    cylinder(r=4.2/2,h=32);
                }
            }
        }
    }

    // photosensor holes
    translate([70,-1,15]){
        rotate([-90,0,0]){
            cylinder(r=3/2,h=12);
        }
    }

    // version
    translate([50,1,5]){
        rotate([90,-90,0]){
            linear_extrude(h=2){
                text(VERSION);
            }
        }
    }
}