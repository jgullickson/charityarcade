$fn=50;

difference(){
    translate([2,-2,0]){
        #cube([51,50,37]);
    }

	// cut corner
    translate([35,-2,-1]){
        cube([20,18.5,40]);
    }
	translate([35,-40,-1]){
		rotate([0,0,45]){
			cube([40,40,42]);
		}
	}

	// cut-out for display corner
	translate([-1,11,-1]){
		cube([30,40,32]);
	}

	// screw holes
	translate([15,5,-1]){
		// thread
		cylinder(r=2.5,h=40);

		translate([0,0,10]){
			// head
			cylinder(r=5,h=30);
		}
	}
    
	translate([45,25,-1]){
		// thread
		cylinder(r=2.5,h=40);

		translate([0,0,10]){
			// head
			cylinder(r=5,h=30);
		}
	}
    
	translate([45,40,-1]){
		// thread
		cylinder(r=2.5,h=40);

		translate([0,0,10]){
			// head
			cylinder(r=5,h=30);
		}
	}
    
    // version
    translate([10,15,35]){
        //rotate([90,0,0]){
            linear_extrude(h=5){
                text("V5");
            }
        //}
    }
}