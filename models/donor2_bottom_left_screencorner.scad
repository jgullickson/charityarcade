$fn=50;

difference(){
    translate([-18,-2,0]){
        cube([61,40,35]);
    }
/*
	// cut corner
	translate([-8,0,-1]){
		rotate([0,0,45]){
			#cube([60,40,42]);
		}
	}
*/
	// cut-out for display corner
	translate([-22,11,-1]){
		#cube([50,30,30]);
	}

	// screw holes
	translate([-6,5,-1]){
		// thread
		cylinder(r=2.5,h=40);

		translate([0,0,10]){
			// head
			cylinder(r=5,h=30);
		}
	}
    
	// screw holes
	translate([15,5,-1]){
		// thread
		cylinder(r=2.5,h=40);

		translate([0,0,10]){
			// head
			cylinder(r=5,h=30);
		}
	}

	translate([35,5,-1]){
		// thread
		cylinder(r=2.5,h=40);

		translate([0,0,10]){
			// head
			cylinder(r=5,h=30);
		}
	}

	translate([35,25,-1]){
		// thread
		cylinder(r=2.5,h=40);

		translate([0,0,10]){
			// head
			cylinder(r=5,h=30);
		}
	}
}