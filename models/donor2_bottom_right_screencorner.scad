$fn=50;

difference(){
    translate([2,-2,0]){
        cube([41,60,35]);
    }
/*
	// cut corner
	translate([-8,0,-1]){
		rotate([0,0,45]){
			#cube([60,40,42]);
		}
	}
*/
	// cut-out for display corner
	translate([-1,11,-1]){
		cube([30,50,30]);
	}

	// screw holes
	translate([15,5,-1]){
		// thread
		cylinder(r=2.5,h=40);

		translate([0,0,10]){
			// head
			cylinder(r=5,h=30);
		}
	}

	translate([35,5,-1]){
		// thread
		cylinder(r=2.5,h=40);

		translate([0,0,10]){
			// head
			cylinder(r=5,h=30);
		}
	}

	translate([35,25,-1]){
		// thread
		cylinder(r=2.5,h=40);

		translate([0,0,10]){
			// head
			cylinder(r=5,h=30);
		}
	}
    
	translate([35,45,-1]){
		// thread
		cylinder(r=2.5,h=40);

		translate([0,0,10]){
			// head
			cylinder(r=5,h=30);
		}
	}
	
}